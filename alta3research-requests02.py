#!/usr/bin/python3
"""Author - Naga Maneesh Kolisetti
Description:
    Accessing University details using 'universities.hipolabs' API.
APIs:
    1) Accessing university details based on name
        http://universities.hipolabs.com/search?name=middle
    2) Accessing university details based on name and country
        http://universities.hipolabs.com/search?name=middle&country=turkey
"""
# standard library
import json

# External library
# python3 -m pip install requests
import requests

# Define the base URL
URL = "http://universities.hipolabs.com/search?"


# search for all universities contaning name in different country
def find_university(name, country=None):
    '''
    This function triggers API request for given name and/or country and return list of universities in JSON format
    :param name:name of university to search
    :param country:country to search
    :return:JSON with list of universities
    '''
    try:
        api = f"{URL}name={name}"
        if country:
            api = f"{URL}name={name}" + f"&country={country}"

        resp = requests.get(api)
        return resp.json()
    except:
        return False


def main():
    while True:
        # initialize answer
        answer = ""
        while answer == "":
            print("""\n****Please select an option to find out details about Universities****\n
            1) Search for All Universities with name
            2) Search for All Universities with name in a specific country
            99) Exit\n""")

            answer = input("Please select an option : ")

        resp = None
        if answer in ["1", "2"]:
            name = input("Enter University name you want to search : ")
            name = name.lower()

            if answer == "1":
                resp = find_university(name)

            if answer == "2":
                country = input('''Enter Country name you want to search. E.g, United States, United Kingdom, Iran
                                Default country is India :''') or "india"
                country = country.lower()
                resp = find_university(name, country)

            if resp:
                print("\n *** Universities matching your search criteria are listed below *** \n")
                for uni in resp:
                    print("University Name : {}".format(uni.get('name')))
                    print("State/Province : {}".format(uni.get('state-province')))
                    if not None:
                        print("Website : {}".format(uni.get('web_pages')[0]))
                    else:
                        print("Website : {}".format(uni.get('web_pages')))
                    print("Country : {}".format(uni.get('country')))
                    print("\n")
            else:
                print("We did not find any Universities with your search criteria")

        elif answer == "99":
            print("Thanks for using our Unversity Search")
            break
        
        else:
            print("Invalid Input. Please select anyone from below options")

if __name__ == '__main__':
    main()
