#!/usr/bin/python3
"""Author - Naga Maneesh Kolisetti
Description:
    Accessing US Population details using 'https://datausa.io/'
APIs:
    1) Home Page - http://10.4.189.153:2225/
    1) Get Population for entire country - http://10.4.189.153:2225/country
    2) Get Population for all states in US - http://10.4.189.153:2225/state
"""
# standard library
import json
import sqlite3 as sql

# External library
# python3 -m pip install flask
import requests

# python3 -m pip install flask
from flask import Flask
from flask import redirect
from flask import url_for
from flask import render_template
from flask import request

app = Flask(__name__)
# return homepage.html (landing page)
@app.route('/')
def homepage():
    return render_template('homepage.html')

#return United States Country Population details
@app.route('/country')
def countrydetails():
    con = sql.connect("database.db")
    con.row_factory = sql.Row
    cur = con.cursor()
    cur.execute("SELECT * from country")  # pull all information from the table "country"
    rows = cur.fetchall()
    con.close()
    return render_template("country.html", rows=rows)

#return United States Population details - State wise
@app.route('/state')
def statedetails():
    con = sql.connect("database.db")
    con.row_factory = sql.Row
    cur = con.cursor()
    cur.execute("SELECT * from state")  # pull all information from the table "country"
    rows = cur.fetchall()
    con.close()
    return render_template("state.html", rows=rows)

if __name__ == '__main__':
    try:
        con = sql.connect('database.db')
        con.execute('CREATE TABLE IF NOT EXISTS country (Country TEXT, Population NUMBER, Year NUMBER)')
        con.execute('CREATE TABLE IF NOT EXISTS state (State TEXT, Population NUMBER, Year NUMBER)')

        cur = con.cursor()
        cur.execute('SELECT COUNT(*) from country')
        country_result = cur.fetchone()

        if country_result[0] == 0:
            country_resp = requests.get('https://datausa.io/api/data?drilldowns=Nation&measures=Population')
            country_records = [(country.get('Nation'), country.get('Population'), country.get('Year')) for country in country_resp.json()['data']]
            cur.executemany('INSERT INTO country VALUES(?,?,?);', country_records);

        cur.execute('SELECT COUNT(*) from state')
        state_result = cur.fetchone()
        if state_result[0] == 0:
            state_resp = requests.get('https://datausa.io/api/data?drilldowns=State&measures=Population')
            state_records = [(state.get('State'), state.get('Population'), state.get('Year')) for state in state_resp.json()['data']]
            cur.executemany('INSERT INTO state VALUES(?,?,?);', state_records);

        con.commit()
        con.close()
        app.run(host="0.0.0.0", port=2225, debug = True)
    except:
        print("App failed on boot")
